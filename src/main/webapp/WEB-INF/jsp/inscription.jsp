<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Java EE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
    	form > div {
    		padding: .5em;
    	}
    	
    	div > label:first-child {
    		display: inline-block;
			min-width: 18em;
		}
		
		.error {
			color: red;
		}
    </style>
  </head>
  <body>
	<div class="container">
		<form method="post" accept-charset="utf-8">
			<div>
				<label for="nom">Nom : </label>
				<input id="nom" name="nom" type="text" placeholder="Entrez votre nom" value="<c:out value="${param['nom']}" />"> 
				<span class="error"><c:out value="${errors['nom']}"/></span>
			</div>
			<div>
				<label for="prenom">Prénom : </label>
				<input id="prenom" name="prenom" placeholder="Entrez votre prénom"  type="text">
				<span class="error"><c:out value="${errors['prenom']}"/></span>
			</div>
			<div>
				<p>Selectionnez votre année d'inscription :</p>
				<select name="promotion">
					<c:forEach var="name"  items="${list}" >
			          <option value="${name}"> ${name} </option>
					</c:forEach>
				</select>
			</div>
			<div>
				<input id="approbation" name="approbation" value="true" type="checkbox">
				<label for="approbation">J'ai lu et approuvé les conditions générales de ce site</label>
				<span class="error"><c:out value="${errors['approbation']}"/></span>
			</div>
			<div>
				<button type="submit" class="btn btn-primary">S'inscrire</button>
			</div>
		</form>
		<div>
			<a href="<c:url value="/"/>"><button type="submit" class="btn btn-light">Retour à l'accueil</button></a>
		</div>
	</div>

  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>