<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Java EE</title>
  </head>
  <body>
  	<div class="container">
		<div>Votre inscription a bien été prise en compte le 
			 <fmt:formatDate type="date" dateStyle="long" value="${inscription.date}"/> à 
			 <fmt:formatDate type="time" value="${inscription.date}"/>
			 </br>
			 <h5>Nom de l'étudiant : </h5><c:out value="${inscription.nom}"/>-<c:out value="${inscription.prenom}"/>.
			 </br>
			 <h5>Promotion : </h5><c:out value="${inscription.promotion}"/>.
			 
		</div>
		<div>
			<h5>Pour cette promotion les étudiants, voici les étudiants déjà inscrits :</h5>
			<c:out value="${etudiant}"/>
		</div>
		</br>
		</br>
		<div>
		  	<a href="<c:url value="/"/>"><button type="submit" class="btn btn-light">Retour à l'accueil</button></a>  
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
