package inscription.modele;

import java.util.Date;

public class Inscription {

	private String nom;
	private String prenom;
	private String promotion;
	private Date date;
	
	public Inscription(String nom, String prenom, String promotion) {
		this.nom = nom;
		this.prenom = prenom;
		this.promotion = promotion;
		this.date = new Date();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getPromotion() {
		return promotion;
	}
	
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
