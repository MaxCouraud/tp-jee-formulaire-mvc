package inscription.modele;

import java.util.ArrayList;
import java.util.List;

public class InscriptionService {
	
	public Inscription inscrire(String nom, String prenom, String promotion, boolean approbation) throws InscriptionInvalideException {
		InscriptionInvalideException ex = new InscriptionInvalideException();
		
		if (nom == null || nom.length() < 2) {
			ex.addMessage("nom", "Nom invalide !");
		}
		if (prenom == null || prenom.length() < 2) {
			ex.addMessage("prenom", "Prenom Invalide !");
		}
		if (! approbation) {
			ex.addMessage("approbation", "Vous devez accepter les conditions.");
		}
		if (ex.mustBeThrown()) {
			throw ex;
		}
		
		return new Inscription(nom, prenom, promotion);
	}
	 public List<String> getList() {
	      List<String> list = new ArrayList<String>();
	      list.add("Bachelor niveau 1");
	      list.add("Bachelor niveau 2");
	      list.add("Bachelor niveau 3");
	      list.add("Bachelor niveau 4");
	      list.add("Bachelor niveau 5");
	      return list;
	 }
}
