	package inscription.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import inscription.modele.Inscription;
import inscription.modele.InscriptionInvalideException;
import inscription.modele.InscriptionService;

@WebServlet("/inscription")
public class InscriptionControleurServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> combobox = new InscriptionService().getList();
		req.setAttribute("list", combobox);
		List<String> etudiants = new ArrayList<>();
		getServletContext().setAttribute("etudiant", etudiants);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
		rd.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nom = req.getParameter("nom");
		String prenom = req.getParameter("prenom");
		String promotion = req.getParameter("promotion");
		boolean approbation = Boolean.valueOf(req.getParameter("approbation"));
		try {
			List<String> combobox = new InscriptionService().getList();
			req.setAttribute("list", combobox);
			InscriptionService inscriptionService = new InscriptionService();
			Inscription inscription = inscriptionService.inscrire(nom, prenom, promotion, approbation);
			
			List<String> etudiants = (List<String>) this.getServletContext().getAttribute("etudiant");
			etudiants.add(inscription.getPrenom()+" "+inscription.getNom());
			getServletContext().setAttribute("etudiant", etudiants);
			
			req.setAttribute("inscription", inscription);
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/validationInscription.jsp");
			rd.forward(req, resp);
		} catch (InscriptionInvalideException e) {
			List<String> combobox = new InscriptionService().getList();
			req.setAttribute("list", combobox);
			req.setAttribute("errors", e.getMessages());
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
			rd.forward(req, resp);
		}
	}
}
